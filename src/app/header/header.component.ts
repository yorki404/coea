import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private titleService: Title) { }

  isNavbarCollapsed =  false;

  ngOnInit() {

    this.titleService.setTitle("COEA");

  }

}
