import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    let homeMenu = document.getElementById("about-menu");
    homeMenu.children[0].classList.add('active');
    homeMenu.classList.remove('cool-link');

  }

}
