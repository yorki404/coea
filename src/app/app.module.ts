import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPopper } from 'angular-popper';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FlickityModule } from 'ngx-flickity';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { WhoweareComponent } from './whoweare/whoweare.component';
import { InitiativesComponent } from './initiatives/initiatives.component';
import { MessageComponent } from './message/message.component';

const routes: Routes = [

	{path:'header',component:HeaderComponent},
	{path:'',component:HomeComponent},
	{path:'footer',component:FooterComponent},
	{path:'contact',component:ContactComponent},
	{path:'who-we-are',component:WhoweareComponent},
	{path:'initiatives',component:InitiativesComponent},
	{path:'message',component:MessageComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ContactComponent,
    WhoweareComponent,
    InitiativesComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
		RouterModule.forRoot(routes,
    { useHash: true }
  	),
    NgbModule,
		NgxPopper,
		MDBBootstrapModule.forRoot(),
		FlickityModule,
		SlickCarouselModule,
		BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
