import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    let contactMenu = document.getElementById("contact-menu");
    contactMenu.classList.remove('cool-link');

  }

}
