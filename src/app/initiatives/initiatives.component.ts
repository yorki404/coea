import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-initiatives',
  templateUrl: './initiatives.component.html',
  styleUrls: ['./initiatives.component.css']
})
export class InitiativesComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    let homeMenu = document.getElementById("about-menu");
    homeMenu.children[0].classList.add('active');
    homeMenu.classList.remove('cool-link');

  }

}
